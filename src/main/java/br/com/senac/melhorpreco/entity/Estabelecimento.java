/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.melhorpreco.entity;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author wilso
 */
@Entity
public class Estabelecimento implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id ;
    
    @Column(nullable = false, length = 50)
    private String nome;
    
    @Column(nullable = false)
   
    private Calendar horario_inicio;
    
    
    @Column(nullable = false)
    
     private Calendar horario_fim;

    public Estabelecimento() {
    }

    public Estabelecimento(int id, String nome, Calendar horario_inicio, Calendar horario_fim) {
        this.id = id;
        this.nome = nome;
        this.horario_inicio = horario_inicio;
        this.horario_fim = horario_fim;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Calendar getHorario_inicio() {
        return horario_inicio;
    }

    public void setHorario_inicio(Calendar horario_inicio) {
        this.horario_inicio = horario_inicio;
    }

    public Calendar getHorario_fim() {
        return horario_fim;
    }

    public void setHorario_fim(Calendar horario_fim) {
        this.horario_fim = horario_fim;
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
