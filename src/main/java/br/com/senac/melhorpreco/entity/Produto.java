/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.melhorpreco.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author wilso
 */
@Entity
public class Produto implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id ;
    
    @Column(nullable = false)
    private int codigo ;
    
    @Column(nullable = false, length = 50)
    private String marca ;
    
     @Column(nullable = false, length = 50)
    private String descricao ;
     
     
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date validade ;

    public Produto() {
    }

    public Produto(int id, int codigo, String marca, String descricao, Date validade) {
        this.id = id;
        this.codigo = codigo;
        this.marca = marca;
        this.descricao = descricao;
        this.validade = validade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getValidade() {
        return validade;
    }

    public void setValidade(Date validade) {
        this.validade = validade;
    }
    

    
    
    
}
