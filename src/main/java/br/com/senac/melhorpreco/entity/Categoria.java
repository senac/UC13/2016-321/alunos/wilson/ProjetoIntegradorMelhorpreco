/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.melhorpreco.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author wilso
 */
@Entity
public class Categoria implements Serializable{
    
 @Id   
 @GeneratedValue(strategy = GenerationType.AUTO)   
 private int id;
 
 @Column(nullable = false, length = 50)
 private String bebida; 

    public Categoria() {
    }

    public Categoria(int id, String bebida) {
        this.id = id;
        this.bebida = bebida;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBebida() {
        return bebida;
    }

    public void setBebida(String bebida) {
        this.bebida = bebida;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
