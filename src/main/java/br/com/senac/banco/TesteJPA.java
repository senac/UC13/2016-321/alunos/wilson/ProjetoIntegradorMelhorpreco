/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;


import br.com.senac.melhorpreco.entity.Cliente;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author wilso
 */
public class TesteJPA {
    
    public static void main(String... a) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Melhor_Preco");
        
        EntityManager em = emf.createEntityManager();
        
        
        Cliente cliente = new Cliente();
        cliente.setNome("Wilson de oliveira");
        cliente.setEmail("Wilsondeoliveira@gmail");
        cliente.setTelefone("3323332222");
        
       
        
        try {
            em.getTransaction().begin();
            em.persist(cliente);
            em.getTransaction().commit();
            System.out.println("Codigo:" + cliente.getId() + "Nome" + "Email"+ "Telefone" );
       
        
        } catch (Exception e) {
            em.getTransaction().rollback();
        }finally{
            em.close();
        }
        
        
        
        
        
        
        
        
        
        
        
    }
    
}
