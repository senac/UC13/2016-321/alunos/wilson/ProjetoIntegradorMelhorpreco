/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import br.com.senac.melhorpreco.entity.Categoria;

/**
 *
 * @author wilso
 */
public class CategoriaDAO extends DAO<Categoria>{
    public CategoriaDAO(){
        super(Categoria.class);
    }
}
