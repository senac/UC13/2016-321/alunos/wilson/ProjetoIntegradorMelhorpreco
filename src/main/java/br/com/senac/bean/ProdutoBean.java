/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.banco.ProdutoDAO;
import br.com.senac.melhorpreco.entity.Produto;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.view.ViewScoped;

/**
 *
 * @author wilso
 */
@Named(value = "produtoBean")
@ViewScoped
public class ProdutoBean extends Bean {

    private Produto produto;
    private ProdutoDAO dao;

    public ProdutoBean() {
    }

    @PostConstruct
    public void init() {
        this.produto = new Produto();
        this.dao = new ProdutoDAO();

    }

    public String getCodigo() {
        return this.produto.getId() == 0 ? "" : String.valueOf(this.produto.getId());
    }

    public void novo() {
        this.produto = new Produto();
    }

    public void salvar() {
        try {

            if (this.produto.getId() == 0) {
                dao.save(produto);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(produto);
                addMessageInfo("Alterado com sucesso!");
            }

        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }

    }

    public void excluir(Produto produto) {
        try {

            dao.delete(produto.getId());
            addMessageInfo("Removido com sucesso!");

        } catch (Exception ex) {
            addMessagemErro(ex.getMessage());
        }

    }
    
    
    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }
    
    public List<Produto> getLista(){
        return this.dao.findAll();
    }
    
    
    
    
    
    
    
    

}
