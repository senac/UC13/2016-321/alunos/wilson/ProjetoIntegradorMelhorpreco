/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.banco.EstabelecimentoDAO;
import br.com.senac.melhorpreco.entity.Estabelecimento;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author wilso
 */
@Named(value = "estabelecimentoBean")
@ViewScoped
public class EstabelecimentoBean extends Bean {
    
    private Estabelecimento estabelecimento;
    private EstabelecimentoDAO dao;
    
    public EstabelecimentoBean() {
    }
    
    @PostConstruct
    public void init() {
        this.estabelecimento = new Estabelecimento();
        this.dao = new EstabelecimentoDAO();
    }
    
    public String getCodigo() {
        return this.estabelecimento.getId() == 0 ? "" : String.valueOf(this.estabelecimento.getId());
    }
    
    public void novo() {
        this.estabelecimento = new Estabelecimento();
    }
    
    public void salvar() {
        try {
            if (this.estabelecimento.getId() == 0) {
                dao.save(estabelecimento);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(estabelecimento);
                addMessageInfo("Alterado com sucesso!");
            }            
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
        
    }
    
    public void excluir (Estabelecimento estabelecimento){
        try {
            dao.delete(estabelecimento.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessagemErro(ex.getMessage());
        }
    }

    public Estabelecimento getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(Estabelecimento estabelecimento) {
        this.estabelecimento = estabelecimento;
    }
    
    public List<Estabelecimento> getLista(){
        return this.dao.findAll();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
