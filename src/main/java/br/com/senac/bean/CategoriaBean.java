/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.banco.CategoriaDAO;
import br.com.senac.melhorpreco.entity.Categoria;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author wilso
 */
@Named(value = "categoriaBean")
@ViewScoped
public class CategoriaBean extends Bean{
    
    private Categoria categoria;
    private CategoriaDAO dao;

    public CategoriaBean() {
    }
    
    @PostConstruct
    public void init(){
        this.categoria = new Categoria();
        this.dao = new CategoriaDAO();
    }
    
    public String getCodigo(){
        return this.categoria.getId() == 0 ? "" : String.valueOf(this.categoria.getId());
    }
    
    public void novo(){
        this.categoria = new Categoria();
    }
    
    
    public void salvar(){
        try {
            
            if(this.categoria.getId() == 0){
                dao.save(categoria);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(categoria);
                addMessageInfo("Alterado com sucesso!");
            }
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
    }
    
    
    public void excluir(Categoria categoria){
        try {
            dao.delete(categoria.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessagemErro(ex.getMessage());
        }
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
    
    public List<Categoria> getLista(){
        return this.dao.findAll();
    }
    
    
    
    
    
}
