/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.banco.EnderecoDAO;
import br.com.senac.melhorpreco.entity.Endereco;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author wilso
 */
@Named(value = "enderecoBean")
@ViewScoped
public class EnderecoBean extends Bean {
    
    private Endereco endereco;
    private EnderecoDAO dao;
    
    public EnderecoBean() {
    }
    
    @PostConstruct
    public void init() {
        this.endereco = new Endereco();
        this.dao = new EnderecoDAO();
    }
    
    public String getCodigo() {
        return this.endereco.getId() == 0 ? "" : String.valueOf(this.endereco.getId());
    }
    
    public void novo() {
        this.endereco = new Endereco();
    }    
    
    public void salvar() {
        try {
            
            if (this.endereco.getId() == 0) {
                dao.save(endereco);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(endereco);
                addMessageInfo("Alterado com sucesso!");
            }
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
    }
    
    public void excluir(Endereco endereco){
        try {
            dao.delete(endereco.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessagemErro(ex.getMessage());
        }
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
    
    public List<Endereco> getLista(){
        return this.dao.findAll();
    }
    
    
    
    
    
    
    
    
    
    
    
}
