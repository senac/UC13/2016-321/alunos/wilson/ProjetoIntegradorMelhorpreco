/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.bean;

import br.com.senac.banco.OfertaDAO;
import br.com.senac.melhorpreco.entity.Oferta;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;

import javax.inject.Named;

/**
 *
 * @author wilso
 */
@Named(value = "ofertaBean")
@ViewScoped
public class OfertaBean extends Bean{
    private Oferta oferta;
    private OfertaDAO dao;

    public OfertaBean() {
    }
    
    
    @PostConstruct
    public void init(){
        this.oferta = new Oferta();
        this.dao = new OfertaDAO();
    }
    
    public String getCodigo(){
        return this.oferta.getId() == 0 ? "" : String.valueOf(this.oferta.getId());
    }
    
    public void novo(){
        this.oferta = new Oferta();
    }
    
    public void salvar(){
        try {
          if(this.oferta.getId() == 0){
              dao.save(oferta);
              addMessageInfo("Salvo com sucesso!");
          } else {
              dao.update(oferta);
              addMessageInfo("Alterado com sucesso!");
          }  
             
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
    }
    
    public void excluir(Oferta oferta){
        try {
            dao.delete(oferta.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessagemErro(ex.getMessage());
        }
    }

    public Oferta getOferta() {
        return oferta;
    }

    public void setOferta(Oferta oferta) {
        this.oferta = oferta;
    }
    
    
    
   public List<Oferta> getLista(){
       return this.dao.findAll();
   }
    
    
    
    
    
    
    
    
    
    
    
    
}
